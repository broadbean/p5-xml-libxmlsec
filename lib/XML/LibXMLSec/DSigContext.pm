package XML::LibXMLSec::DSigContext;
use strict;
use warnings;
use XML::LibXMLSec qw();

our $VERSION = '0.009';

=head1 NAME

XML::LibXMLSec::DSigContext - XML digital signature context

=head1 DESCRIPTION

This class wraps the L<<
C<xmldsig>|https://www.aleksey.com/xmlsec/api/xmlsec-xmldsig.html >>
portion of C<xmlsec>.

=head1 METHODS

=head2 C<new>

  my $dsig = XML::LibXMLSec::DSigContext->new();

  my $dsig = XML::LibXMLSec::DSigContext->new($key_manager);

Builds a new context, optionally tying it to a L<<
C<XML::LibXMLSec::KeysManager> >>.

=head2 C<enabled_reference_uris>

  $dsig->enabled_reference_uris(
    XML::LibXMLSec::Transform::URI_TYPE_SAME_DOCUMENT() |
    XML::LibXMLSec::Transform::URI_TYPE_LOCAL()
  );

Sets the C<enabledReferenceUris> field. This is a bit field, its value
should be obtained by C<or>-ing together the C<URI_TYPE_*> constants
from L<< C<XML::LibXMLSec::Transform>|XML::LibXMLSec::Transform/URI
types >>.

=head2 C<verify>

  my $status = $dsig->verify($xml_node);

Given a signature node (you can use L<<
C<XML::LibXMLSec::Utils::find_node>|XML::LibXMLSec::Utils/find_node >>
to get it from an arbitrary XML document), returns a status code (see
the L</signature status> constants).

This function I<does not return a boolean>!

=head1 CONSTANTS

=head2 Signature status

These constants are returned by L<< /C<verify> >>.

=head3 C<STATUS_UNKNOWN>

Something went wrong, we don't know if the signature is valid or not.

=head3 C<STATUS_SUCCEEDED>

The signature is valid.

=head3 C<STATUS_INVALID>

The signature is not valid.

=cut

sub STATUS_UNKNOWN() { 0 }
sub STATUS_SUCCEEDED() { 1 }
sub STATUS_INVALID() { 2 }

=head1 SEE ALSO

L<https://www.aleksey.com/xmlsec/api/xmlsec-xmldsig.html> the original
documentation.

=cut

1;
