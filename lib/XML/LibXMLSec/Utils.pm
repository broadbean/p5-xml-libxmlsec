package XML::LibXMLSec::Utils;
use strict;
use warnings;
use XML::LibXMLSec qw();

our $VERSION = '0.009';

=head1 NAME

XML::LibXMLSec::Utils - utility functions and constants

=head1 DESCRIPTION

This module provides some useful functions and constants for use with
C<xmlsec>. Nothing is exported, you should use the fully qualified
name of the functions to call them.

=head1 FUNCTIONS

=head2 C<add_id_attr>

  XML::LibXMLSec::Utils::add_id_attr(
    $xml_doc,
    $node_namespace_uri,
    $node_name,
    $attr_name,
  );

This function scans the XML document, looking for nodes with the given
namespace and local name. If any such node exists, and has an
attribute of the given name, that attribute is marked as an "ID".

This is useful, for example, when you have a signed XML document, but
no DTD, and the signature contains things like C<< <ds:Reference
URI="#id84193712402627052114698066"> >>. That reference won't work
unless the corresponding attribute has been marked as an ID. In those
cases you'd do:

    XML::LibXMLSec::Utils::add_id_attr(
        $document,
        'urn:oasis:names:tc:SAML:2.0:protocol','Response',
        'ID',
    );

(This example works for SAML assertions).

=head2 C<find_node>

  my $xml_node = XML::LibXMLSec::Utils::find_node(
    $xml_doc,
    $node_namespace_uri,
    $node_name,
  );

Scans the given document, and returns the first node (in document
order) with the given namespace and local name.

=head1 CONSTANTS

=head2 XML Namespaces

=head2 C<NS>

The string C<http://www.aleksey.com/xmlsec/2002>

=head2 C<DSIG_NS>

The string C<http://www.w3.org/2000/09/xmldsig#>

=head2 C<ENC_NS>

The string C<http://www.w3.org/2001/04/xmlenc#>

=head2 C<XKMS_NS>

The string C<http://www.w3.org/2002/03/xkms#>

=head2 C<XPATH_NS>

The string C<http://www.w3.org/TR/1999/REC-xpath-19991116>

=head2 C<XPATH2_NS>

The string C<http://www.w3.org/2002/06/xmldsig-filter2>

=head2 C<XPOINTER_NS>

The string C<http://www.w3.org/2001/04/xmldsig-more/xptr>

=head2 C<SOAP11_NS>

The string C<http://schemas.xmlsoap.org/soap/envelope/>

=head2 C<SOAP12_NS>

The string C<http://www.w3.org/2002/06/soap-envelope>

=head2 XML Digital Signature node names

=head2 C<NODE_SIGNATURE>

The string C<Signature>

=head2 C<NODE_SIGNED_INFO>

The string C<SignedInfo>

=head2 C<NODE_CANONICALIZATION_METHOD>

The string C<CanonicalizationMethod>

=head2 C<NODE_SIGNATURE_METHOD>

The string C<SignatureMethod>

=head2 C<NODE_SIGNATURE_VALUE>

The string C<SignatureValue>

=head2 C<NODE_DIGEST_METHOD>

The string C<DigestMethod>

=head2 C<NODE_DIGEST_VALUE>

The string C<DigestValue>

=head2 C<NODE_OBJECT>

The string C<Object>

=head2 C<NODE_MANIFEST>

The string C<Manifest>

=head2 C<NODE_SIGNATURE_PROPERTIES>

The string C<SignatureProperties>

=head2 XML Encryption node names

=head2 C<NODE_ENCRYPTED_DATA>

The string C<EncryptedData>

=head2 C<NODE_ENCRYPTION_METHOD>

The string C<EncryptionMethod>

=head2 C<NODE_ENCRYPTION_PROPERTIES>

The string C<EncryptionProperties>

=head2 C<NODE_ENCRYPTION_PROPERTY>

The string C<EncryptionProperty>

=head2 C<NODE_CIPHER_DATA>

The string C<CipherData>

=head2 C<NODE_CIPHER_VALUE>

The string C<CipherValue>

=head2 C<NODE_CIPHER_REFERENCE>

The string C<CipherReference>

=head2 C<NODE_REFERENCE_LIST>

The string C<ReferenceList>

=head2 C<NODE_DATA_REFERENCE>

The string C<DataReference>

=head2 C<NODE_KEY_REFERENCE>

The string C<KeyReference>

=head2 C<NODE_CARRIED_KEY_NAME>

The string C<CarriedKeyName>

=head1 SEE ALSO

L<https://www.aleksey.com/xmlsec/api/xmlsec-transforms.html> the
original documentation.

=for Pod::Coverage
TYPE_ENC_CONTENT
TYPE_ENC_ELEMENT

=cut

1;
