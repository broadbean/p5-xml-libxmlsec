package XML::LibXMLSec::KeysManager;
use strict;
use warnings;
use XML::LibXMLSec qw();

our $VERSION = '0.009';

=head1 NAME

XML::LibXMLSec::KeysManager - the key manager class

=head1 DESCRIPTION

This class wraps the L<<
C<keysmngr>|https://www.aleksey.com/xmlsec/api/xmlsec-keysmngr.html >>
portion of C<xmlsec>.

=head1 METHODS

=head2 C<new>

  my $manager = XML::LibXMLSec::KeysManager->new();

Builds a default keys manager.

=head2 C<load_certificate_from_string>

  $manager->load_certificate_from_string(
    $data_string,
    $key_format,
    $key_type,
  );

This method loads a certificate (or other key data) from a string.

C<$key_format> must be one of the formats defined in L<<
C<XML::LibXMLSec::KeysData>|XML::LibXMLSec::KeysData/Key format >>

C<$key_type> must be a bitmask obtained by C<or>-ing together the
types defined in L<<
C<XML::LibXMLSec::KeysData>|XML::LibXMLSec::KeysData/Key type >>

=head1 SEE ALSO

L<https://www.aleksey.com/xmlsec/api/xmlsec-keysmngr.html> the
original documentation.

=cut

1;
