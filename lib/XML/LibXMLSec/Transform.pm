package XML::LibXMLSec::Transform;
use strict;
use warnings;

our $VERSION = '0.009';

=head1 NAME

XML::LibXMLSec::Transform - XML transforms

=head1 DESCRIPTION

Currently this module only holds some constants.

=head1 CONSTANTS

=head2 URI types

These are bits masks that determines acceptable URI transformations.

=head2 C<URI_TYPE_NONE>

The URI type is unknown or not set.

=head2 C<URI_TYPE_EMPTY>

The empty URI ("") type.

=head2 C<URI_TYPE_SAME_DOCUMENT>

The same document (C<#...>) but not empty ("") URI type.

=head2 C<URI_TYPE_LOCAL>

The local URI (C<file:///....>) type.

=head2 C<URI_TYPE_REMOTE>

The remote URI type.

=head2 C<URI_TYPE_ANY>

Any URI type.

=cut

sub URI_TYPE_NONE() { 0x0000 }
sub URI_TYPE_EMPTY() { 0x0001 }
sub URI_TYPE_SAME_DOCUMENT() { 0x0002 }
sub URI_TYPE_LOCAL() { 0x0004 }
sub URI_TYPE_REMOTE() { 0x0008 }
sub URI_TYPE_ANY() { 0xFFFF }

1;
