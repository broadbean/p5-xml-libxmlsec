package XML::LibXMLSec::KeysData;
use strict;
use warnings;

our $VERSION = '0.009';

=head1 NAME

XML::LibXMLSec::KeysData - key data

=head1 DESCRIPTION

Currently this module only holds some constants.

=head1 CONSTANTS

=head2 Key usage

These are bits masks that determines possible keys data usage.

=head2 C<USAGE_UNKNOWN>

The key data usage is unknown.

=head2 C<USAGE_KEY_INFO_NODE_READ>

The key data could be read from a C<< <dsig:KeyInfo/> >> child.

=head2 C<USAGE_KEY_INFO_NODE_WRITE>

The key data could be written to a C<< <dsig:KeyInfo /> >> child.

=head2 C<USAGE_KEY_VALUE_NODE_READ>

The key data could be read from a C<< <dsig:KeyValue /> >> child.

=head2 C<USAGE_KEY_VALUE_NODE_WRITE>

The key data could be written to a C<< <dsig:KeyValue /> >> child.

=head2 C<USAGE_RETRIEVAL_METHOD_NODE_XML>

The key data could be retrieved using C<< <dsig:RetrievalMethod /> >>
node in XML format.

=head2 C<USAGE_RETRIEVAL_METHOD_NODE_BIN>

The key data could be retrieved using C<< <dsig:RetrievalMethod /> >>
node in binary format.

=head2 C<USAGE_ANY>

Any key data usage.

=cut

sub USAGE_UNKNOWN() { 0x00000 }
sub USAGE_KEY_INFO_NODE_READ() { 0x00001 }
sub USAGE_KEY_INFO_NODE_WRITE() { 0x00002 }
sub USAGE_KEY_VALUE_NODE_READ() { 0x00004 }
sub USAGE_KEY_VALUE_NODE_WRITE() { 0x00008 }
sub USAGE_RETRIEVAL_METHOD_NODE_XML() { 0x00010 }
sub USAGE_RETRIEVAL_METHOD_NODE_BIN() { 0x00020 }
sub USAGE_ANY() { 0xFFFFF }

=head2 Key type

The key data type (public/private, session/permanet, etc.).

=head2 C<TYPE_UNKNOWN>

The key data type is unknown (same as L<< /C<TYPE_NONE> >>).

=head2 C<TYPE_NONE>

The key data type is unknown (same as L<< /C<TYPE_UNKNOWN> >>).

=head2 C<TYPE_PUBLIC>

The key data contain a public key.

=head2 C<TYPE_PRIVATE>

The key data contain a private key.

=head2 C<TYPE_SYMMETRIC>

The key data contain a symmetric key.

=head2 C<TYPE_SESSION>

The key data contain session key (one time key, not stored in keys
manager).

=head2 C<TYPE_PERMANENT>

The key data contain permanent key (stored in keys manager).

=head2 C<TYPE_TRUSTED>

The key data is trusted.

=head2 C<TYPE_ANY>

Any key data.

=cut

sub TYPE_UNKNOWN() { 0x0000 }
sub TYPE_NONE() { 0x0000 }
sub TYPE_PUBLIC() { 0x0001 }
sub TYPE_PRIVATE() { 0x0002 }
sub TYPE_SYMMETRIC() { 0x0004 }
sub TYPE_SESSION() { 0x0008 }
sub TYPE_PERMANENT() { 0x0010 }
sub TYPE_TRUSTED() { 0x0100 }
sub TYPE_ANY() { 0xFFFF }

=head2 Key format

The key data format (binary, der, pem, etc.).

=head2 C<FORMAT_UNKNOWN>

The key data format is unknown.

=head2 C<FORMAT_BINARY>

The key data format is binary.

=head2 C<FORMAT_PEM>

The key data format is PEM (cert or public/private key).

=head2 C<FORMAT_DER>

The key data format is DER (cert or public/private key).

=head2 C<FORMAT_PKCS8_PEM>

The key is a PKCS#8 PEM private key.

=head2 C<FORMAT_PKCS8_DER>

The key is a PKCS#8 DER private key.

=head2 C<FORMAT_PKCS12>

The key is in PKCS#12 format (bag of keys and certs)

=head2 C<FORMAT_CERT_PEM>

The key is a certificate, in PEM format.

=head2 C<FORMAT_CERT_DER>

The key is a certificate, in DER format.

=cut

sub FORMAT_UNKNOWN() { 0 }
sub FORMAT_BINARY() { 1 }
sub FORMAT_PEM() { 2 }
sub FORMAT_DER() { 3 }
sub FORMAT_PKCS8_PEM() { 4 }
sub FORMAT_PKCS8_DER() { 5 }
sub FORMAT_PKCS12() { 6 }
sub FORMAT_CERT_PEM() { 7 }
sub FORMAT_CERT_DER() { 8 }

=head1 SEE ALSO

L<https://www.aleksey.com/xmlsec/api/xmlsec-keysdata.html> the
original documentation.

=cut

1;
