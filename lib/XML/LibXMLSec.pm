package XML::LibXMLSec;
use strict;
use warnings;
use XML::LibXML;
use Alien::LibXMLSec;

our $VERSION = '0.009';
require XSLoader;
XSLoader::load('XML::LibXMLSec', $VERSION);
INIT_THREAD_SUPPORT() if XML::LibXML::threads_shared_enabled();

=head1 NAME

XML::LibXMLSec - bindings to some of xmlsec1

=head1 SYNOPSIS

  use XML::LibXMLSec::Simple;

  if (XML::LibXMLSec::Simple::verify(
        $xml_libxml_document,
        $pem_certificate_string,
      )) {
    ...
  }

=head1 DESCRIPTION

This distribution wraps a few functions from C<xmlsec1>. At the
moment, only the code needed to verify a XML Signature against a PEM
certificate is implemented, to be used as in the synopsis.

=head1 SEE ALSO

L<https://www.aleksey.com/xmlsec/index.html> the C<xmlsec> library

=head1 AUTHOR

Gianni Ceccarelli <gianni.ceccarelli@broadbean.com>

=for Pod::Coverage
INIT_THREAD_SUPPORT

=cut

1;
