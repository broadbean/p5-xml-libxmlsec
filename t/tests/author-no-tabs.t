
BEGIN {
  unless ($ENV{AUTHOR_TESTING}) {
    require Test::More;
    Test::More::plan(skip_all => 'these tests are for testing by the author');
  }
}

use strict;
use warnings;

# this test was generated with Dist::Zilla::Plugin::Test::NoTabs 0.15

use Test::More 0.88;
use Test::NoTabs;

my @files = (
    'lib/XML/LibXMLSec.pm',
    'lib/XML/LibXMLSec/Transform.pm',
    'lib/XML/LibXMLSec/KeysData.pm',
    'lib/XML/LibXMLSec/Simple.pm',
    'lib/XML/LibXMLSec/Utils.pm',
    'lib/XML/LibXMLSec/DSigContext.pm',
    'lib/XML/LibXMLSec/KeysManager.pm',
);

notabs_ok($_) foreach @files;
done_testing;
