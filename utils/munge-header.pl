#!/usr/bin/env perl
use strict;
use warnings;

sub c2p {
    return uc($_[0] =~ s{([a-z])([A-Z])}{${1}_${2}}gr);
}

my $last_v;
while (<>) {
    /^#define\s+(\w+)\s+(\S+)\s*$/ && do {
        my ($n,$v) = ($1,$2);$n=c2p($n);
        print qq{sub $n() { $v }\n};
        next;
    };
    /^\s*(\w+)(?:\s*=\s*(\S+?))?,?\s*$/ && do {
        my ($n,$v) = ($1,$2);$n=c2p($n);
        if (defined $v) {
            $last_v = $v;
        }
        else {
            $v = ++$last_v;
        }
        print qq{sub $n() { $v }\n};
        next;
    };
    /^XMLSEC_EXPORT_VAR const xmlChar (\w+)\[\]/ && do {
        my $n = $1;my $pn=c2p($n);
        print <<"XS";
const char*
$pn()
  CODE:
   RETVAL = $n
  OUTPUT:
   RETVAL

XS
        next;
    };
}
