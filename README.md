`XML::LibXMLSec` - wrapper for *some* of xmlsec1

This distribution wraps a few functions from `xmlsec1`.

At the moment, only the code needed to verify a XML Signature against
a PEM certificate is implemented.

This distribution depends on `Alien::LibXMLSec` to get the C
library. That one, in turn, depends on `libxml2` to be installed,
including the development headers.

The build instructions are the usual ones:

    perl Makefile.PL
    make test
    make install
