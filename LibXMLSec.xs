#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <string.h>

#include <libxml/tree.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

#include <xmlsec/xmlsec.h>
#include <xmlsec/xmltree.h>
#include <xmlsec/xmldsig.h>
#include <xmlsec/crypto.h>

#ifndef XMLSEC_NO_XSLT
#include <libxslt/xslt.h>
#include <libxslt/security.h>
#endif /* XMLSEC_NO_XSLT */

#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include "perl-libxml-mm.h"
#include "ppport.h"

#ifdef __cplusplus
}
#endif

SV* x_PROXY_NODE_REGISTRY_MUTEX = NULL;

/* copied from
   https://www.aleksey.com/xmlsec/api/xmlsec-notes-verify.html
   https://www.aleksey.com/xmlsec/api/xmlsec-verify-with-key.html#XMLSEC-EXAMPLE-VERIFY1
*/

static int
xmlSecAppAddIDAttr(xmlNodePtr node, const xmlChar* attrName, const xmlChar* nodeName, const xmlChar* nsHref) {
    xmlAttrPtr attr, tmpAttr;
    xmlNodePtr cur;
    xmlChar* id;

    if((node == NULL) || (attrName == NULL) || (nodeName == NULL)) {
        return(-1);
    }

    /* process children first because it does not matter much but does simplify code */
    cur = xmlSecGetNextElementNode(node->children);
    while(cur != NULL) {
        if(xmlSecAppAddIDAttr(cur, attrName, nodeName, nsHref) < 0) {
            return(-1);
        }
        cur = xmlSecGetNextElementNode(cur->next);
    }

    /* node name must match */
    if(!xmlStrEqual(node->name, nodeName)) {
        return(0);
    }

    /* if nsHref is set then it also should match */
    if((nsHref != NULL) && (node->ns != NULL) && (!xmlStrEqual(nsHref, node->ns->href))) {
        return(0);
    }

    /* the attribute with name equal to attrName should exist */
    for(attr = node->properties; attr != NULL; attr = attr->next) {
        if(xmlStrEqual(attr->name, attrName)) {
            break;
        }
    }
    if(attr == NULL) {
        return(0);
    }

    /* and this attr should have a value */
    id = xmlNodeListGetString(node->doc, attr->children, 1);
    if(id == NULL) {
        return(0);
    }

    /* check that we don't have same ID already */
    tmpAttr = xmlGetID(node->doc, id);
    if(tmpAttr == NULL) {
        xmlAddID(NULL, node->doc, id, attr);
    } else if(tmpAttr != attr) {
        warn("duplicate ID attribute \"%s\"\n", id);
        xmlFree(id);
        return(-1);
    }
    xmlFree(id);
    return(0);
}

MODULE = XML::LibXMLSec  PACKAGE = XML::LibXMLSec PREFIX = xml_libxmlsec

PROTOTYPES: DISABLE

BOOT:
  /* copied from
     https://www.aleksey.com/xmlsec/api/xmlsec-notes-verify.html
     ttps://www.aleksey.com/xmlsec/api/xmlsec-verify-with-key.html#XMLSEC-EXAMPLE-VERIFY1
  */
  if (xmlSecInit() < 0) {
    croak("xmlsec initialization failed");
  }
  if (xmlSecCheckVersion() != 1) {
    croak("loaded xmlsec library version is not compatible");
  }
  /* Load default crypto engine if we are supporting dynamic
     * loading for xmlsec-crypto libraries. Use the crypto library
     * name ("openssl", "nss", etc.) to load corresponding 
     * xmlsec-crypto library.
     */
#ifdef XMLSEC_CRYPTO_DYNAMIC_LOADING
  if (xmlSecCryptoDLLoadLibrary(BAD_CAST XMLSEC_CRYPTO) < 0) {
    croak("unable to load default xmlsec-crypto library");
  }
#endif /* XMLSEC_CRYPTO_DYNAMIC_LOADING */
  if (xmlSecCryptoAppInit(NULL) < 0) {
    croak("crypto app initialization failed");
  }
  if (xmlSecCryptoInit() < 0) {
      croak("xmlsec-crypto initialization failed");
  }

void
INIT_THREAD_SUPPORT()
    CODE:
       /* copied from XML::LibXSLT */
       if (x_PROXY_NODE_REGISTRY_MUTEX != NULL) {
	  croak("XML::LibXSLT::INIT_THREAD_SUPPORT can only be called once!\n");
       }
       x_PROXY_NODE_REGISTRY_MUTEX = get_sv("XML::LibXML::__PROXY_NODE_REGISTRY_MUTEX",0);

MODULE = XML::LibXMLSec  PACKAGE = XML::LibXMLSec::Utils

void
add_id_attr(doc, node_ns, node_name, attr_name)
  xmlDocPtr doc
  const char* node_ns
  const char* node_name
  const char* attr_name
 PREINIT:
  xmlNodePtr cur = NULL;
 CODE:
  if (xmlSecAppAddIDAttr((xmlNodePtr)doc, attr_name, node_name, node_ns) < 0) {
    croak("failed to add ID attribute \"%s\" for node \"{%s}%s\"\n", attr_name, node_ns, node_name);
  }

SV*
find_node(parent,node_name,node_ns)
  xmlNodePtr parent
  const char* node_ns
  const char* node_name
 PREINIT:
  xmlNodePtr found = NULL;
 CODE:
  found = xmlSecFindNode(parent,node_name,node_ns);
  if (!found) {
    XSRETURN_UNDEF;
  }
  RETVAL = x_PmmNodeToSv(found, x_PmmOWNERPO(PmmPROXYNODE(parent)));
 OUTPUT:
  RETVAL

const char*
NS()
  CODE:
   RETVAL = xmlSecNs;
  OUTPUT:
   RETVAL

const char*
DSIG_NS()
  CODE:
   RETVAL = xmlSecDSigNs;
  OUTPUT:
   RETVAL

const char*
ENC_NS()
  CODE:
   RETVAL = xmlSecEncNs;
  OUTPUT:
   RETVAL

const char*
XPATH_NS()
  CODE:
   RETVAL = xmlSecXPathNs;
  OUTPUT:
   RETVAL

const char*
XPATH2_NS()
  CODE:
   RETVAL = xmlSecXPath2Ns;
  OUTPUT:
   RETVAL

const char*
XPOINTER_NS()
  CODE:
   RETVAL = xmlSecXPointerNs;
  OUTPUT:
   RETVAL

const char*
SOAP11_NS()
  CODE:
   RETVAL = xmlSecSoap11Ns;
  OUTPUT:
   RETVAL

const char*
SOAP12_NS()
  CODE:
   RETVAL = xmlSecSoap12Ns;
  OUTPUT:
   RETVAL

const char*
NODE_SIGNATURE()
  CODE:
   RETVAL = xmlSecNodeSignature;
  OUTPUT:
   RETVAL

const char*
NODE_SIGNED_INFO()
  CODE:
   RETVAL = xmlSecNodeSignedInfo;
  OUTPUT:
   RETVAL

const char*
NODE_SIGNATURE_VALUE()
  CODE:
   RETVAL = xmlSecNodeSignatureValue;
  OUTPUT:
   RETVAL

const char*
NODE_CANONICALIZATION_METHOD()
  CODE:
   RETVAL = xmlSecNodeCanonicalizationMethod;
  OUTPUT:
   RETVAL

const char*
NODE_SIGNATURE_METHOD()
  CODE:
   RETVAL = xmlSecNodeSignatureMethod;
  OUTPUT:
   RETVAL

const char*
NODE_DIGEST_METHOD()
  CODE:
   RETVAL = xmlSecNodeDigestMethod;
  OUTPUT:
   RETVAL

const char*
NODE_DIGEST_VALUE()
  CODE:
   RETVAL = xmlSecNodeDigestValue;
  OUTPUT:
   RETVAL

const char*
NODE_OBJECT()
  CODE:
   RETVAL = xmlSecNodeObject;
  OUTPUT:
   RETVAL

const char*
NODE_MANIFEST()
  CODE:
   RETVAL = xmlSecNodeManifest;
  OUTPUT:
   RETVAL

const char*
NODE_SIGNATURE_PROPERTIES()
  CODE:
   RETVAL = xmlSecNodeSignatureProperties;
  OUTPUT:
   RETVAL

const char*
NODE_ENCRYPTED_DATA()
  CODE:
   RETVAL = xmlSecNodeEncryptedData;
  OUTPUT:
   RETVAL

const char*
NODE_ENCRYPTION_METHOD()
  CODE:
   RETVAL = xmlSecNodeEncryptionMethod;
  OUTPUT:
   RETVAL

const char*
NODE_ENCRYPTION_PROPERTIES()
  CODE:
   RETVAL = xmlSecNodeEncryptionProperties;
  OUTPUT:
   RETVAL

const char*
NODE_ENCRYPTION_PROPERTY()
  CODE:
   RETVAL = xmlSecNodeEncryptionProperty;
  OUTPUT:
   RETVAL

const char*
NODE_CIPHER_DATA()
  CODE:
   RETVAL = xmlSecNodeCipherData;
  OUTPUT:
   RETVAL

const char*
NODE_CIPHER_VALUE()
  CODE:
   RETVAL = xmlSecNodeCipherValue;
  OUTPUT:
   RETVAL

const char*
NODE_CIPHER_REFERENCE()
  CODE:
   RETVAL = xmlSecNodeCipherReference;
  OUTPUT:
   RETVAL

const char*
NODE_REFERENCE_LIST()
  CODE:
   RETVAL = xmlSecNodeReferenceList;
  OUTPUT:
   RETVAL

const char*
NODE_DATA_REFERENCE()
  CODE:
   RETVAL = xmlSecNodeDataReference;
  OUTPUT:
   RETVAL

const char*
NODE_KEY_REFERENCE()
  CODE:
   RETVAL = xmlSecNodeKeyReference;
  OUTPUT:
   RETVAL

const char*
NODE_CARRIED_KEY_NAME()
  CODE:
   RETVAL = xmlSecNodeCarriedKeyName;
  OUTPUT:
   RETVAL

const char*
TYPE_ENC_CONTENT()
  CODE:
   RETVAL = xmlSecTypeEncContent;
  OUTPUT:
   RETVAL

const char*
TYPE_ENC_ELEMENT()
  CODE:
   RETVAL = xmlSecTypeEncElement;
  OUTPUT:
   RETVAL

MODULE = XML::LibXMLSec PACKAGE = XML::LibXMLSec::KeysManager

xmlSecKeysMngrPtr
new(CLASS)
  CODE:
    RETVAL = xmlSecKeysMngrCreate();
    if (RETVAL == NULL) {
      croak("failed to create key manager");
    }
    if (xmlSecCryptoAppDefaultKeysMngrInit(RETVAL) < 0) {
      xmlSecKeysMngrDestroy(RETVAL);
      croak("failed to init key manager");
    }
  OUTPUT:
    RETVAL

void
DESTROY(self)
   xmlSecKeysMngrPtr self
  CODE:
   xmlSecKeysMngrDestroy(self);

void
load_certificate_from_string(self,cert,format,type)
   xmlSecKeysMngrPtr self
   SV* cert
   xmlSecKeyDataFormat format
   xmlSecKeyDataType type
  PREINIT:
   char *key_data;STRLEN key_len;
  CODE:
   key_data = SvPV(cert,key_len);
   if (xmlSecCryptoAppKeysMngrCertLoadMemory(
     self,
     key_data,key_len,
     format,
     type) < 0) {
     croak("failed to load the certificate");
   }
   
MODULE = XML::LibXMLSec PACKAGE = XML::LibXMLSec::DSigContext

xmlSecDSigCtxPtr
new(CLASS,keymanager)
  xmlSecKeysMngrPtr keymanager
  PREINIT:
    xmlSecDSigCtxPtr dsigCtx = NULL;
  CODE:
    RETVAL = xmlSecDSigCtxCreate(keymanager);
    if (RETVAL == NULL) {
      croak("failed to create signature context");
    }
  OUTPUT:
    RETVAL

void
DESTROY(self)
   xmlSecDSigCtxPtr self
  CODE:
   xmlSecDSigCtxDestroy(self);

void
enabled_reference_uris(self,tranform_type)
  xmlSecDSigCtxPtr self
  xmlSecTransformUriType tranform_type
 CODE:
  self->enabledReferenceUris = tranform_type;
     
int
verify(self,node)
   xmlSecDSigCtxPtr self
   xmlNodePtr node
  PREINIT:
    int res;
  CODE:
    res = xmlSecDSigCtxVerify(self, node);
    if (res < 0) {
      croak("signature verify internal error");
    }
    RETVAL = self->status;
  OUTPUT:
    RETVAL
